from django.conf.urls import include, url, patterns
from django.contrib import admin

from django.contrib.auth import views as auth_views

admin.autodiscover()

urlpatterns = patterns('job_system.views',
                       url(r'^$', 'home'),
                       url(r'^accounts/login/$', 'login_page', name="login_page"),
                       url(r'^register/$', 'register_page', name="register_page"),
                       url(r'^profile/$', 'profile_page', name="profile_page"),
                       url(r'^logout/$', 'logout_page', name='logout_page'),
                       url(r'^admin/', include(admin.site.urls)),
                       )

urlpatterns += patterns('job_application.views',
                        url(r'post/job/$', 'post_job'),
                        url(r'post/register/$', 'register_user'),
                        url(r'post/update/profile/$', 'update_user'),
                        )
urlpatterns += patterns('employee.views',
                        url(r'employee/jobs/$', 'view_employee_jobs'),
                        url(r'post/search/employee/$', 'match_employee'),
                        url(r'get/employee/$', 'get_employee'),
                        url(r'send/employee/message/$', 'send_employee_message'),
                        url(r'jobs/employee/$', 'jobs_employee'),
                        )
urlpatterns += patterns('employer.views',
                        url(r'employer/jobs/$', 'view_employer_jobs'),
                        url(r'employer/jobs/(.+)$', 'view_job'),
                        )

handler404 = "job_system.views.not_found_page"
handler500 = "job_system.views.server_error_page"
