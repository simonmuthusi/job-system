from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from job_application.tasks import diagnostics

from job_application.models import Job_Category, Skills
from employee.models import Applicant


@login_required
def home(request):
    template_dict = {}
    # redirect all employees to their profile page
    if request.user.user_info.user_type == "EMPLOYEE":
        return redirect(request.GET.get('next', '/profile/'))

    template_dict['categories'] = Job_Category.objects.all()
    # need to load all skills
    # ','.join(Skills.objects.values_list('name', flat=True))
    # template_dict['skills'] = ""

    template_dict['user_type'] = request.user.user_info.user_type
    return render(request, "cards/home.html", template_dict)


def login_page(request):
    """
    Handles logging in of an administrative user.
    """

    if request.user.is_authenticated():
        return redirect(request.GET.get('next', '/'))

    if request.method == "POST":
        # login_user(request, request.POST.get('username', ''),
        #            request.POST.get('password', ''))
        user = authenticate(username=request.POST.get('username', ''),
                            password=request.POST.get('password', ''))
        if user is not None:
            if user.is_active:
                login(request, user)
                if user.user_info.user_type == "EMPLOYEE":
                    return redirect('/profile/')
                else:
                    return redirect(request.GET.get('next', '/'))
            else:
                return render(request, "user/login.html",
                              {'login_error': 'Disabled Account'})
        else:
            return render(request, "user/login.html",
                          {'login_error': 'Login Failed'})

    return render(request, "user/login.html", {})


def login_user(request, username, password, redirect_url="/"):
    user = authenticate(username=username,
                        password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return redirect(request.GET.get('next', redirect_url))
        else:
            return render(request, "user/login.html",
                          {'login_error': 'Disabled Account'})
    else:
        return render(request, "user/login.html",
                      {'login_error': 'Login Failed'})


def register_page(request):
    """
    Handles registering of an administrative user.
    """
    template_dict = {}
    if request.user.is_authenticated():
        return redirect(request.GET.get('next', '/'))

    return render(request, "user/register.html", template_dict)


@login_required
def profile_page(request):
    """
    Handles the profile page of a user
    """
    template_dict = {}
    template_dict['categories'] = Job_Category.objects.all()
    template_dict['skills'] = Applicant.objects.get(
        employee__user=request.user).get_skills
    template_dict['user'] = request.user
    return render(request, "user/profile.html", template_dict)


def logout_page(request):
    """
    Handles logging out of an administrative user.
    """

    logout(request)
    return redirect("/")


def not_found_page(request):
    """
    A page for handling 404 errors.
    """
    return HttpResponse("404 Not Found", status=404)


def server_error_page(request):
    """
    A page for handling 500 errors.
    """
    return HttpResponse("500 Server Error", status=500)
