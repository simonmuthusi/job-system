from celery import task
# from celery import Celery
from django.core.management import call_command


@task
def diagnostics():
    """
    run diagnostics to remove invalid job seekers
    """

    call_command("run_diagnostics")
