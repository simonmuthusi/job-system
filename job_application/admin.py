from django.contrib import admin
from job_application.models import Job, Job_Category, Skills, Error_Log


class JobAdmin(admin.ModelAdmin):
    list_display = ('title', 'location', 'experience', 'work_type',)
    list_filter = ('status', 'location', 'work_type', 'experience',)


class Job_CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)


class SkillsAdmin(admin.ModelAdmin):
    list_display = ('name',)


class Error_LogAdmin(admin.ModelAdmin):
    list_display = ('error_type', 'error_message',)

admin.autodiscover()
admin.site.register(Job, JobAdmin)
admin.site.register(Job_Category, Job_CategoryAdmin)
admin.site.register(Skills, SkillsAdmin)
admin.site.register(Error_Log, Error_LogAdmin)
