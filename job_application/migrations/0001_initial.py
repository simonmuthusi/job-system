# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-08-27 09:33
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Error_Log',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('error_type', models.CharField(choices=[('INTERNAL', 'Internal Error'), ('EXTERNAL', 'External Error'), ('UNKNOWN', 'Unknown Error')], default='INTERNAL', max_length=15)),
                ('error_message', models.CharField(max_length=1000)),
            ],
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('location', models.CharField(max_length=30)),
                ('experience', models.CharField(max_length=30)),
                ('work_type', models.CharField(max_length=30)),
                ('education', models.CharField(blank=True, max_length=300, null=True)),
                ('skills', models.CharField(blank=True, max_length=300, null=True)),
                ('responsibilities', models.CharField(blank=True, max_length=1000, null=True)),
                ('salary', models.CharField(blank=True, max_length=100, null=True)),
                ('publish_salary', models.CharField(blank=True, max_length=100, null=True)),
                ('how_to_apply', models.CharField(choices=[('SELF', 'Job System'), ('EMAIL', 'Forward to Email Address'), ('EXTERNAL', 'External Link')], default='SELF', max_length=10)),
                ('application_link', models.CharField(blank=True, max_length=100, null=True)),
                ('company_profile', models.CharField(blank=True, max_length=100, null=True)),
                ('date_posted', models.DateTimeField(default=datetime.datetime(2016, 8, 27, 9, 33, 2, 755376, tzinfo=utc))),
                ('go_live_date', models.DateTimeField(default=datetime.datetime(2016, 8, 27, 9, 33, 2, 755412, tzinfo=utc))),
                ('status', models.CharField(choices=[('DRAFT', 'Draft'), ('PUBLISHED', 'Published'), ('EXPIRED', 'Expired')], default='DRAFT', max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Job_Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Skills',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
            ],
        ),
        migrations.AddField(
            model_name='job',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='job_application.Job_Category'),
        ),
    ]
