from django.shortcuts import render
from django.contrib.auth.decorators import login_required
import json
from django.http import HttpResponse
from .models import Job, Error_Log, Job_Category, Skills
from django.contrib.auth.models import User
from employer.models import User_Info, Company
from employee.models import Applicant
from django.core.mail import get_connection, EmailMultiAlternatives
# profile dashboard
PROFILE_URL = "/profile/"


@login_required
def post_job(request):
    """
    Handles posting of job
    """
    template_dict = {}
    post = request.POST
    errors = ""
    if post.get("step") == "step-1":
        if len(post.get("title", "")) < 3 or len(post.get("title", "")) > 100:
            errors += ", Invalid job title length"
        if len(post.get("location", "")) < 3:
            errors += ", Invalid location given"
        if len(post.get("job_level", "")) < 3:
            errors += ", Invalid job level given"
        if len(post.get("experience", "")) < 3:
            errors += ", Invalid experience given"
    # validate for step-p2
    if post.get("step") == "step-2":
        if len(post.get("education", "")) < 3:
            errors += ", Invalid education given"
        if len(post.get("skills", "")) < 3:
            errors += ", Invalid skills given"
        if len(post.get("responsibilities", "")) < 3:
            errors += ", Invalid responsibilities given"
        if len(post.get("publish_salary", "")) < 3:
            errors += ", Invalid publish salary given"

    # validate for step-3
    if post.get("step") == "step-3":
        if len(post.get("company_profile", "")) < 3:
            errors += ", Invalid company profile given"

    if errors == "":
        template_dict['errors'] = False
        # Save data now
        template_dict['saved_job'] = save_job_data(request.user, post)
    else:
        template_dict['errors'] = errors

    return HttpResponse(json.dumps(template_dict))


def save_job_data(login_user, post):
    job = None
    if post.get("step") == "step-1":
        if post.get("saved_job") == "":
            try:
                user_info = User_Info.objects.get(user=login_user)
                try:
                    # create a company profile
                    company = Company.objects.get(contact_person=user_info)
                except:
                    company = Company.objects.create(contact_person=user_info)

                job_category = Job_Category.objects.get(name=post['category'])

                job = Job.objects.create(title=post.get("title"), location=post.get("location"), work_type=post.get(
                    "job_level"), experience=post.get("experience"), category=job_category, company=company)
                

            except Exception as e:
                Error_Log.objects.create(
                    error_type="INTERNAL", error_message=str(e))
                pass
        else:
            try:
                job = Job.objects.get(id=post.get("saved_job"))
                job.title = post.get("title")
                job.location = post.get("location")
                job.work_type = post.get("job_level")
                job.experience = post.get("experience")
                job.save()
            except:
                pass
    elif post.get("step") == "step-2":
        job = Job.objects.get(id=post.get("saved_job")
                              )  # post.get("saved_job")
        job.education = post.get("education")
        # job.skills = post.get("skills")
        job.responsibilities = post.get("responsibilities")
        job.salary = post.get("from_salary", "") + "-" + \
            post.get("to_salary", "")
        job.publish_salary = post.get("publish_salary")
        job.save()

        # save skills
        for skill in post['skills'].split(","):
            try:
                skill = Skills.objects.get(name__iexact=skill)
            except:
                skill = Skills.objects.create(name=skill)
            # save
            job.skills.add(skill)
            job.save()

    elif post.get("step") == "step-3":
        job = Job.objects.get(id=post.get("saved_job")
                              )  # post.get("saved_job")
        company = Company.objects.get(contact_person__user=login_user)
        job.how_to_apply = post.get("application_method")
        job.application_link = post.get("application_link", "")
        job.company = company
        job.save()

    elif post.get("step") == "step-4":
        job = Job.objects.get(id=post.get("saved_job"))
        job.status = "PUBLISHED"
        job.save()

    try:
        category = job.category.name
    except AttributeError:
        category = None
    job_details = {
        "job_id": job.id,
        "title": job.title,
        "location": job.location,
        "experience": job.experience,
        'work_type': job.work_type,
        "category": category,
        "education": job.education,
        "skills": job.get_skills,
        "responsibilities": job.responsibilities,
        "salary": job.salary,
        "publish_salary": job.publish_salary,
        "how_to_apply": job.how_to_apply,
        "application_link": job.application_link,
        "company_profile": job.company.get_profile,
        "status": job.status
    }

    return job_details


def register_user(request):
    template_dict = {}
    post = request.POST
    # validate data received
    errors = ""
    if len(post.get("email", "")) < 3:
        errors += "invalid email provided,"
    # check whether user exists
    try:
        login_user = User.objects.get(username=post.get("email", ""))
        errors = "user already exists"
    except:
        pass
    # create user now
    if errors == "":
        template_dict = create_user(post)
    else:
        template_dict['error'] = errors

    return HttpResponse(json.dumps(template_dict))


def create_user(post):
    user_errors = {}
    user_errors['error'] = True
    user_errors['success'] = False
    try:
        login_user = User.objects.create(
            username=post['email'],
            email=post['email'],
            is_staff=False, is_active=True)

        # create User_Info profile
        user_info_profile = User_Info.objects.create(
            user=login_user, user_type=post['account-type'])

        if post['account-type'] == "EMPLOYER":
            user_profile = Company.objects.create(
                contact_person=user_info_profile)
            user_errors['success'] = "Created successfully"
            user_errors['error'] = False
            user_errors['redirect_url'] = PROFILE_URL
        elif post['account-type'] == "EMPLOYEE":
            user_profile = Applicant.objects.create(employee=user_info_profile)
            user_errors['success'] = "Created successfully"
            user_errors['error'] = False
            user_errors['redirect_url'] = PROFILE_URL
        else:
            user_errors['error'] = "Invalid user provided"
    except Exception as e:
        try:
            user_info_profile.delete()
            login_user.delete()
        except:
            pass
        user_errors['error'] = "Error occured: " + str(e)

    # send login information
    # Generate new passwords
    if user_errors['error'] == False:
        password = User.objects.make_random_password()

        login_user.set_password(password)
        login_user.save()

        subject, from_email, to = 'Job System User Account', 'simonglassfish@gmail.com', login_user.email
        html_content = "<p>Dear " + login_user.email + ",<br/></p><p>Your Job System account has been created. You can now access our services by clicking <a href='http://127.0.0.1:8000'>here</a></p><p>Your login credentials are:<br/>Username: " + login_user.username + "<br/>Password: " + password + \
            "</p><p><b>Password Protection</b><br/>Please keep your passwords private, and remember to log out of your account if you are using a public computer.<br/></p><p><b>Password Change</b><br/> To change your password, click on the 'forgot password' link on the application main page.</p><p></br>If you have any questions, you can write to us at <a href='mailto:simonmuthusi@gmail.com'>simonmuthusi@gmail.com</a>. Thank you and welcome to Job System.</p><p><address>Jobo Team<br/><b>Website: </b>www.sim-on.net</address></p>"
        msg = EmailMultiAlternatives(subject, '', from_email, [to])
        msg.attach_alternative(html_content, "text/html")

        try:
            msg.send()
        except:
            user_errors['error'] = "success, but email not send"

    return user_errors


def send_email(from_email, subject, message, attachment=False):
    """
    send email
    """


def update_user(request):
    template_dict = {}
    template_dict['error'] = True
    template_dict['success'] = False

    post = request.POST
    errors = ""

    # validate input
    if len(post.get("first_name", "")) < 3:
        errors += ", Invalid first name given"
    if len(post.get("last_name", "")) < 3:
        errors += ", Invalid last name given"
    if len(post.get("education", "")) < 3:
        errors += ", Invalid education level given"
    if len(post.get("skills", "")) < 3:
        errors += ", Invalid skills given"
    if len(post.get("from_salary", "")) < 4:
        errors += ", Invalid from salary given"
    if len(post.get("to_salary", "")) < 4:
        errors += ", Invalid to salary given"

    if errors == "":
        # update user
        login_user = request.user
        login_user.first_name = post['first_name']
        login_user.last_name = post['last_name']
        login_user.save()

        if login_user.user_info.user_type == "EMPLOYER":
            user_errors[
                'error'] = "updating the wrong user. Kindly refresh your page."
            user_errors['success'] = False
        elif login_user.user_info.user_type == "EMPLOYEE":
            try:
                applicant = Applicant.objects.get(employee__user=login_user)
                applicant.experience = post['experience']
                # applicant.field_of_intrest = post['category']
                # applicant.skills = post['skills']
                applicant.education = post['education']
                applicant.salary = post[
                    'from_salary'] + "-" + post['to_salary']

                # get category
                try:
                    category = Job_Category.objects.get(name=post['category'])
                except:
                    category = Job_Category.objects.create(name=post['category'])
                applicant.category = category

                # save skills
                applicant.skills.all().delete()
                # applicant.skills.clear()
                applicant.save()
                for skill in post['skills'].split(","):
                    try:
                        skill = Skills.objects.get(name__iexact=skill)
                    except:
                        skill = Skills.objects.create(name=skill)
                    applicant.skills.add(skill)
                    applicant.save()

                applicant.save()
                template_dict['error'] = False
                template_dict[
                    'success'] = "updated successfully. Thank you for updating you profile. You can now begin applying for jobs."
            except Exception as e:
                template_dict['error'] = str(e)
                template_dict['success'] = False

        else:
            template_dict['error'] = "invalid user type"
            template_dict['success'] = False

    else:
        template_dict['error'] = errors

    return HttpResponse(json.dumps(template_dict))
