from __future__ import unicode_literals

from django.db import models
from django_countries.fields import CountryField
from django.utils import timezone


class Job(models.Model):
    APPLICATION_TYPES = (
        ("SELF", "Job System"),
        ("EMAIL", "Forward to Email Address"),
        ("EXTERNAL", "External Link"),
    )
    JOB_STATUS = (
        ("DRAFT", "Draft"),
        ("PUBLISHED", "Published"),
        ("EXPIRED", "Expired"),
    )
    title = models.CharField(max_length=100)
    location = models.CharField(max_length=30)  # CountryField()
    experience = models.CharField(max_length=30)
    work_type = models.CharField(max_length=30)

    category = models.ForeignKey(
        'job_application.Job_Category', blank=True, null=True)
    education = models.CharField(max_length=300, null=True, blank=True)
    skills = models.ManyToManyField(
        "job_application.Skills", null=True, blank=True)
    responsibilities = models.CharField(max_length=1000, null=True, blank=True)

    salary = models.CharField(max_length=100, null=True, blank=True)
    publish_salary = models.CharField(max_length=100, null=True, blank=True)
    how_to_apply = models.CharField(max_length=10, choices=APPLICATION_TYPES,
                                    default="SELF")
    application_link = models.CharField(max_length=100, null=True, blank=True)
    # change this to get from logged in user
    company = models.ForeignKey("employer.Company", blank=True, null=True)

    date_posted = models.DateTimeField(default=timezone.now())
    go_live_date = models.DateTimeField(default=timezone.now())
    status = models.CharField(
        max_length=10, choices=JOB_STATUS, default="DRAFT")

    @property
    def get_skills(self):
        # returns a string of skills for applicant
        my_skills = list()
        if self.skills:
            for skill in self.skills.all():
                my_skills.append(skill.name)

        return ",".join(my_skills)


class Job_Category(models.Model):
    """
    Defines job categories
    """
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class Skills(models.Model):
    """
    Defines job skills
    """
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class Error_Log(models.Model):
    """
    Defines error logs for the system
    """
    ERROR_TYPES = (
        ("INTERNAL", "Internal Error"),
        ("EXTERNAL", "External Error"),
        ("UNKNOWN", "Unknown Error"),
    )
    error_type = models.CharField(
        max_length=15, choices=ERROR_TYPES, default="INTERNAL")
    error_message = models.CharField(max_length=1000)

    def __unicode__(self):
        return self.error_type + "-"+self.error_message
