from django.core.management.base import BaseCommand
from django.utils import timezone
from datetime import timedelta
from job_system.settings import JOB_VALIDITY

from employee.models import Applicant
from job_application.models import Error_Log


class Command(BaseCommand):

    def add_arguments(self, parser):
        print "no arguments"

    def handle(self, *args, **options):
        """
        remove unwanted upplicants
        -- applicant becomes unwanted if they haven't filled their profile for a period of 14 days from account creation
        Format : manage.py run_diagnostics
        """
        date_limit = timezone.now() - timedelta(days=JOB_VALIDITY)

        applicants = list(Applicant.objects.filter(timestamp__gte=date_limit))

        for applicant in applicants:
            if not is_valid(applicant):
                try:
                    email = EmailMessage(
                        'Applicant Diagnostic Report', "Applicant deleted: %s %s %s" % (applicant.employee.user.first_name, applicant.employee.user.last_name, applicant.employee.user.username), to=[address])
                except:
                    pass
                applicant.delete()

        def is_valid(applicant):
            """
            checks whether an applicant is valid
            """
            is_valid = True
            try:
                if applicant.employee.user.first_name == "" or applicant.employee.user.first_name == "":
                    is_valid = False
                if len(applicant.skills.all()) == 0:
                    is_valid = False
                if applicant.experience == "" and applicant.education == "":
                    is_valid = False
            except Exception as e:
                Error_Log.objects.create(
                    error_type="INTERNAL", error_message=str(e))
                is_valid = False
