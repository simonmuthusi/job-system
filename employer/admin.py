from django.contrib import admin

from employer.models import User_Info, Company, EmployerJobs


class User_InfoAdmin(admin.ModelAdmin):
    list_display = ('user', 'user_type',)


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'profile','contact_person',)


class EmployerJobsAdmin(admin.ModelAdmin):
    list_display = ('employer', 'job',)

admin.autodiscover()
admin.site.register(User_Info, User_InfoAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(EmployerJobs, EmployerJobsAdmin)
