from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class User_Info(models.Model):
    """
    Extends more employer attibutes to the user object
    """
    USER_TYPES = (
        ("EMPLOYER", "Employer"),
        ("EMPLOYEE", "Job Seeker"),
    )
    user = models.OneToOneField(User)
    user_type = models.CharField(max_length=10, choices=USER_TYPES)

    def __unicode__(self):
        return str(self.user) + "- " + self.user_type


class Company(models.Model):
    name = models.CharField(max_length=40, null=True, blank=True)
    profile = models.CharField(max_length=300, null=True, blank=True)
    no_of_employees = models.CharField(max_length=5, default="1")
    contact_person = models.OneToOneField("employer.User_Info")


    @property
    def get_profile(self):
        profile = ""
        if self.profile:
            profile = self.profile
        return profile


class EmployerJobs(models.Model):
    employer = models.ForeignKey(User)
    job = models.ForeignKey("job_application.Job")
