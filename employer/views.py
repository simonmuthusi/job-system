from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from job_application.models import Job



@login_required
def view_employer_jobs(request):
	# view emploer jobs
    template_dict = {}
    # redirect all employees to their profile page
    if request.user.user_info.user_type == "EMPLOYEE":
        return redirect(request.GET.get('next', '/jobs/employee/'))

    # get all my jobs
    jobs = Job.objects.filter(company__contact_person__user=request.user)

    template_dict['user_type'] = request.user.user_info.user_type
    template_dict['jobs'] = jobs
    return render(request, "cards/employer_jobs.html", template_dict)

@login_required
def view_job(request, job_id):
	# view emploer jobs
    template_dict = {}
    # redirect all employees to their profile page
    if request.user.user_info.user_type == "EMPLOYEE":
        return redirect(request.GET.get('next', '/jobs/employee/'))

    # get all my jobs
    try:
    	job = Job.objects.get(id=job_id, company__contact_person__user=request.user)
    	template_dict['job'] = job
    except:
    	template_dict['job'] = None


    template_dict['user_type'] = request.user.user_info.user_type
    return render(request, "cards/view_job.html", template_dict)


