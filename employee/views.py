from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
import json
from job_application.models import Job
from .models import Applicant
from django.core import serializers
from django.contrib.auth.models import User
from django.core.mail import get_connection, EmailMultiAlternatives


@login_required
def view_employee_jobs(request):
    template_dict = {}
    # redirect all employees to their profile page
    if request.user.user_info.user_type == "EMPLOYER":
        return redirect(request.GET.get('next', '/jobs/employer/'))

    template_dict['user_type'] = request.user.user_info.user_type
    return render(request, "cards/employee_jobs.html", template_dict)


@login_required
def match_employee(request):
    template_dict = {}
    template_dict['error'] = True
    template_dict['success'] = False
    job_id = request.POST.get("job_id", "")
    try:
        job = Job.objects.get(id=job_id)

        applicants_by_skill = Applicant.objects.filter(
            skills__in=job.skills.all()).distinct()

        employee_list = list()
        for applicant in applicants_by_skill:
            # print applicant
            employee_list.append({"first_name": applicant.employee.user.first_name,
                                  "last_name": applicant.employee.user.last_name,
                                  "experience": applicant.experience, "user_id": applicant.employee.user.id})

        # data = serializers.serialize('json', applicants)
        template_dict['employee_list'] = employee_list
        template_dict['error'] = False
    except Exception as e:
        template_dict['error'] = str(e)

    return HttpResponse(json.dumps(template_dict))


@login_required
def get_employee(request):
    """
    get employee details, given user_id
    """
    template_dict = {}
    template_dict['error'] = True
    template_dict['success'] = False
    user = Applicant.objects.get(employee__user__id=request.POST['user_id'])

    try:
        field_of_intrest =  user.field_of_intrest.name
    except:
        field_of_intrest = "None"

    template_dict['user_det'] = {
        "first_name": user.employee.user.last_name,
        "last_name": user.employee.user.first_name,
        "email": user.employee.user.email,
        "experience": user.experience,
        "field_of_intrest": field_of_intrest,
        "skills": user.get_skills,
        "education": user.education,
        "salary": user.salary,
        "user_id": request.POST['user_id'],
    }
    template_dict['error'] = False
    return HttpResponse(json.dumps(template_dict))


def send_employee_message(request):
    """
    send employee a custom message
    """
    template_dict = {}
    template_dict['error'] = True
    template_dict['success'] = False

    # validate message here
    errors = ""
    post = request.POST
    if len(post['message']) < 5:
        errors += ", Invalid message length"
    template_dict['error'] = errors

    if errors == "":
        # if not errors process message
        try:
            login_user = User.objects.get(id=post['user_id'])
            job = Job.objects.get(id=post['job_id'])

            subject, from_email, to = 'Job Application for ' + job.title + \
                ' Response', 'simonglassfish@gmail.com', login_user.email
            html_content = post['message']
            msg = EmailMultiAlternatives(subject, '', from_email, [to])
            msg.attach_alternative(html_content, "text/html")

            try:
                msg.send()
                template_dict['error'] = False
                template_dict['success'] = "Message send successfully"
            except:
                template_dict['error'] = "success, but email not send"

        except Exception as e:
            template_dict['error'] = str(e)

    return HttpResponse(json.dumps(template_dict))
@login_required
def jobs_employee(request):
    template_dict = {}
    # redirect all employees to their profile page
    if request.user.user_info.user_type == "EMPLOYER":
        return redirect(request.GET.get('next', '/jobs/employer/'))

    template_dict['user_type'] = request.user.user_info.user_type
    template_dict['jobs'] = Job.objects.all()
    return render(request, "cards/employee_jobs.html", template_dict)

