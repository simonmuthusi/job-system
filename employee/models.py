from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


from django.contrib.auth.models import User
from employer.models import User_Info
from job_application.models import Job_Category, Skills


class Applicant(models.Model):
    experience = models.CharField(max_length=40, blank=True, null=True)
    field_of_intrest = models.ForeignKey(
        "job_application.Job_Category", blank=True, null=True)
    skills = models.ManyToManyField(
        "job_application.Skills", null=True, blank=True)
    education = models.CharField(max_length=100, blank=True, null=True)
    salary = models.CharField(max_length=100, blank=True, null=True)
    employee = models.OneToOneField("employer.User_Info")
    timestamp = models.DateTimeField(default=timezone.now())

    @property
    def get_skills(self):
        # returns a string of skills for applicant
        my_skills = list()
        if self.skills:
            for skill in self.skills.all():
                my_skills.append(skill.name)

        return ",".join(my_skills)


class ApplicantJobs(models.Model):
    applicant = models.ForeignKey(User)
    job = models.ForeignKey("job_application.Job")
