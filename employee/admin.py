from django.contrib import admin
from employee.models import Applicant


class ApplicantAdmin(admin.ModelAdmin):
    list_display = ('employee', 'experience', 'field_of_intrest',)

admin.autodiscover()
admin.site.register(Applicant, ApplicantAdmin)
