# job-system
A job matching system is an online portal for matching employer jobs to job seekers. It's a django based system.

# Installation
- this installation has been tested on unix (ubuntu) environment. It should however work on major operating systems

1. Before you install, ensure you have the following installed:
  * git - versioning
  * python (preferably 2.7)
  * virtualenv - for creating virtual environments
  * pip - package management
  * rabbitmq-server - message queueing 
2. Steps
  * Download or clone this repository git clone https://github.com/simomuthusi/job-system.git
  * On the downloaded directory, create a virtual environment (virtualenv job-env), and activate through source job-env/bin/activate
  * Install dependancies (pip install -r requirements.txt
  * Start the celery scheduler for background diagnostics (./manage.py celery beat)
  * Start the application through ./manage.py runserver
  * You can also configure your favourite web server (e.g. nginx, apache etc) to run the application in production. see http://goo.gl/z0O7Ta
  
# Features
The following are features of the system
  * Post jobs
  * Matching skills
  
# Framework
The job system uses Django framework.

# Tests
To test, run <i>./manage.py test <app></i> where app is the name of the app to test. 100% test coverage is required for the application.
  
  
# Documentation
See the <a target="_blank" href="https://github.com/simomuthusi/job-system/wiki">wiki</a> section
 
